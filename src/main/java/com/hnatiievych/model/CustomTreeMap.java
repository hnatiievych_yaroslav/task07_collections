package com.hnatiievych.model;

import java.util.*;

public class CustomTreeMap<K extends Comparable<K>, V> implements SimpleTreeMap<K, V> {

    Node root;
    private int size;

    public CustomTreeMap() {
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public V get(Object key) {
        return null;
    }

    @Override
    public V get(K key) {
        Node<K, V> finding = getRecurcive(root, key);
        return finding.value;
    }

    private Node<K, V> getRecurcive(Node current, Object key) {
        if (current.key.compareTo(key) == 1) {
            return current;

        } else if (current.key.compareTo(key) < 0) {
            current.left = getRecurcive(current.left, key);
        } else if (current.key.compareTo(key) > 0) {
            current.right = getRecurcive(current.right, key);
            ;
        }
        return current;
    }


    @Override
    public V put(K key, V value) {
        putRecursive(root, key, value);
        return value;
    }

    private Node putRecursive(Node current, K key, V value) {
        if (current == null) {
            return new Node(key, value);

        } else if (current.key.compareTo(key) < 0) {
            current.left = putRecursive(root.left, key, value);
        } else if (current.key.compareTo(key) > 0) {
            current.right = putRecursive(root.right, key, value);
            ;
        }
        size++;
        return current;

    }

    @Override
    public V remove(Object key) {
        Node<K, V> current = root;
        Node<K, V> parent = root;
        boolean isLeftChild = true;
        while (current.key != key) {
            parent = current;
            if (current.key.compareTo((K) key) < 1) {
                isLeftChild = true;
                current = current.left;
            } else {
                isLeftChild = false;
                current = current.right;
            }
            if (current == null)
                return null;
        }
        if (current.left == null &&
                current.right == null) {
            if (current == root)
                root = null;
            else if (isLeftChild)
                parent.left = null;
            else
                parent.right = null;
        } else if (current.right == null)
            if (current == root)
                root = current.left;
            else if (isLeftChild)
                parent.left = current.left;
            else
                parent.right = current.left;
        else if (current.left == null) {
            if (current == root)
                root = current.right;
            else if (isLeftChild)
                parent.left = current.right;
            else
                parent.right = current.right;
        } else {
            Node successor = getSuccessor(current);
            if (current == root)
                root = successor;
            else if (isLeftChild)
                parent.left = successor;
            else
                parent.right = successor;
            successor.left = current.left;
        }
        return current.getValue();
    }

    private Node getSuccessor(Node delNode) {
        Node successorParent = delNode;
        Node successor = delNode;
        Node current = delNode.right;
        while (current != null) {
            successorParent = successor;
            successor = current;
            current = current.left;
        }
        if (successor != delNode.right) { // make connections
            successorParent.left = successor.right;
            successor.right = delNode.right;
        }
        return successor;
    }


    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public void clear() {

    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry <K, V>> entrySet() {
        Set<Entry<K, V>> set = new HashSet<>();
        runTree(root, set);
        return set;
    }

    private void runTree(Node<K, V> localRoot, Set<Entry<K, V>> set) {
        if (localRoot != null) {
            runTree(localRoot.left, set);
            set.add(localRoot);
            runTree(localRoot.right, set);
        }
    }

    @Override
    public void print() {
    }


    class Node<K extends Comparable<K>, V> implements Map.Entry<K, V> {
        K key;
        V value;
        Node left;
        Node right;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
            left = null;
            right = null;
        }


        @Override
        public K getKey() {
            return this.key;
        }

        @Override
        public V getValue() {
            return this.value;
        }

        @Override
        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }
    }
}

