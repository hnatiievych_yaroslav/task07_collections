package com.hnatiievych.model;

import java.util.Map;

public interface SimpleTreeMap<K extends Comparable<K>,V> extends Map<K,V> {

    V get(K key);

    @Override
    V put(K key, V value);

    @Override
    V remove(Object key);

    void print();
}
