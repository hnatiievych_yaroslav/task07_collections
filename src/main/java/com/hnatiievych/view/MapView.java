package com.hnatiievych.view;

import com.hnatiievych.controller.Controller;
import com.hnatiievych.controller.ControllerImp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MapView implements View {

    private static Logger Logger;
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MapView() {
        Logger = LogManager.getLogger(MapView.class);
        controller = new ControllerImp();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Print simple TreeMap");
        menu.put("2", "  2 - Put into TreeMap");
        menu.put("3", "  3 - Get from TreeMap");
        menu.put("4", "  4 - Remove from TreeMap");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton3);
    }

    private void pressButton1() {
        controller.print();
    }

    private void pressButton2() {
        Logger.info("Put string key and value to add: \n");
        Logger.debug(controller.put(input.nextLine(), input.nextLine()));
    }

    private void pressButton3() {
        Logger.info("Put string key to find: \n");
        Logger.info(controller.get(input.nextLine()));
    }

    private void pressButton4() {
        Logger.info("Put string key to remove: \n");
        Logger.info(controller.remove(input.nextLine()));
    }


    //-------------------------------------------------------------------------

    private void outputMenu() {
        Logger.info("\nMENU:");
        for (String str : menu.values()) {
            Logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            Logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
