package com.hnatiievych.view;

import com.hnatiievych.controller.Controller;
import com.hnatiievych.controller.ControllerImp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class EnumView implements View {
    private static Logger Logger;
    Controller controller;
    private List<MenuItem> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public EnumView() {
        Logger = LogManager.getLogger(EnumView.class);
        controller = new ControllerImp();
        menu = new ArrayList<MenuItem>();
        for (MenuItem item : MenuItem.values())
            menu.add(item);

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton3);
        methodsMenu.put("5", this::pressButton3);
    }

    private void pressButton1() {
        controller.print();
    }

    private void pressButton2() {
        Logger.info("Put string key and value to add: \n");
        Logger.debug(controller.put(input.nextLine(), input.nextLine()));
    }

    private void pressButton3() {
        Logger.info("Put string key to find: \n");
        Logger.info(controller.get(input.nextLine()));
    }

    private void pressButton4() {
        Logger.info("Put string key to remove: \n");
        Logger.info(controller.remove(input.nextLine()));
    }

    private void outputMenu() {
        Logger.info("\nMENU:");
        for (MenuItem item : menu) {
            Logger.info(item.getNumber()+ " - "+ item);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            Logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("5"));
    }

    public enum MenuItem {
        PRINT(1), PUT(2), GET(3), REMOVE(4), EXIT(5);
        int number;

        MenuItem(int i) {
            number = i;
        }

        public int getNumber() {
            return number;
        }
    }
}
