package com.hnatiievych.view;

@FunctionalInterface
public interface Printable {
    public void print();
}
