package com.hnatiievych.view;

public interface View {
    void show();
}
