package com.hnatiievych.controller;

public interface Controller {
    void print();
    Object put(String key, String value);
    Object get(String key);
    Object remove(String key);
}
