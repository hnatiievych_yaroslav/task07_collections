package com.hnatiievych.controller;

import com.hnatiievych.model.CustomTreeMap;
import com.hnatiievych.model.SimpleTreeMap;

public class ControllerImp implements Controller {
    SimpleTreeMap simpleTreeMap;

    public ControllerImp() {
        SimpleTreeMap<String,String> simpleTreeMap=new CustomTreeMap();
        simpleTreeMap.put("first","qweqwe");
        simpleTreeMap.put("second","qwasdasdeqwe");
        simpleTreeMap.put("third","aaqweqwe");
        simpleTreeMap.put("fourth","qgghsweqwe");
    }

    @Override
    public void print() {
        simpleTreeMap.entrySet().forEach(entry->{
            System.out.println(entry.getKey + " " + entry.getValue());
        });;
    }

    @Override
    public Object put(String key, String value) {
        return simpleTreeMap.put(key, value);

    }

    @Override
    public Object get(String key) {
       return simpleTreeMap.get(key);
    }

    @Override
    public Object remove(String key) {
        return simpleTreeMap.remove(key);

    }
}
