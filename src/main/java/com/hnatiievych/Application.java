package com.hnatiievych;

import com.hnatiievych.view.EnumView;
import com.hnatiievych.view.MapView;
import com.hnatiievych.view.View;

public class Application {
    public static void main(String[] args) {
        View view = new EnumView();
        view.show();
    }
}

